package ru.t1.simanov.tm.api.service;

import ru.t1.simanov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
